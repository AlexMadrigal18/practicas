--Seleccion de Columnas desde una tabla

--SELECT last_name, job_id, salary, commission_pct, salary*commission_pct
--FROM hr.employees;
 
 --Alias
 
--SELECT last_name AS apellido,
--commission_pct AS comision
--FROM hr.employees;

--Alias sin usar la instruccion AS

--SELECT last_name"Name",
--salary*12 "Annual Salary"
--FROM hr.employees;

--DESC: Descripcion de tabla, contenidos y tipos de datos

--DESC hr.departments;

--SELECT department_id||'---'|| department_name AS Relacion
--FROM hr.departments;

--SELECT last_name|| ' has a monthly salary of ' || salary || ' dollars.' AS Pay
--FROM hr.employees;

--SELECT last_name ||' has a '|| 1 ||' year salary of '|| salary*12 ||
--' dollars.' AS Pay
--FROM hr.employees;

--SELECT DISTINCT department_id
--FROM hr.employees;

--SELECT employee_id, first_name, last_name
--FROM hr.employees
--WHERE employee_id > 101;

--SELECT first_name, last_name
--FROM employees
--WHERE last_name = 'Taylor';

--SELECT 5*70 FROM dual

--SELECT employee_id, first_name, last_name
--FROM hr.employees
--WHERE hire_date < '01-01-2005'

--SELECT employee_id, first_name, last_name
--FROM hr.employees
--WHERE hire_date < to_date('01-SEP-2005','DD-MON-YYYY');

--SELECT last_name, salary
--FROM hr.employees
--WHERE salary BETWEEN 9000 AND 11000;

--SELECT city, state_province, country_id
--FROM hr.locations
--WHERE country_id IN('UK', 'CA');

--SELECT city, state_province, country_id
--FROM hr.locations
--WHERE country_id = 'UK' OR country_id = 'CA';

--SELECT last_name
--FROM hr.employees
--WHERE last_name LIKE '__a%';

--SELECT last_name, job_id
--FROM hr.EMPLOYEES
--WHERE job_id LIKE '%\_A%' ESCAPE '\';

--SELECT last_name, job_id
--FROM hr.EMPLOYEES
--WHERE job_id LIKE '%_R%'

--SELECT last_name, manager_id
--FROM hr.employees
--WHERE manager_id IS NULL;

--SELECT last_name, commission_pct
--FROM hr.employees
--WHERE commission_pct IS NOT NULL;

--SELECT last_name, department_id, salary
--FROM hr.employees
--WHERE department_id > 50 AND salary > 12000;

--SELECT last_name, hire_date, job_id
--FROM hr.employees
--WHERE hire_date > to_date ('01-ENE-1998','DD-MON-YYYY') AND job_id LIKE 'SA%';

--SELECT department_name, manager_id, location_id
--FROM hr.departments
--WHERE location_id = 2500 OR manager_id=124;

--SELECT department_name, location_id
--FROM hr.departments
--WHERE location_id NOT IN (1700,1800);

--SELECT last_name||' '||salary*1.05 As "Employee Raise"
--FROM hr.employees
--WHERE department_id IN(50,80) AND first_name LIKE 'C%'
--OR last_name LIKE '%s%';

--Orden de Operadores

SELECT last_name||' '||salary*1.05
As "Employee Raise",
department_id, first_name
FROM hr.employees
WHERE department_id IN(50,80)
AND first_name LIKE 'C%'
OR last_name LIKE '%s%';