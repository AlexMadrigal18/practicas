SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) IN
(SELECT EXTRACT(YEAR FROM hire_date)
FROM employees
WHERE department_id=90)
Order by hire_date;

SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) < ANY
(SELECT EXTRACT(YEAR FROM hire_date)
FROM employees
WHERE department_id=90);

SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) > ALL
(SELECT EXTRACT(YEAR FROM hire_date)
FROM employees
WHERE department_id=90);

SELECT department_id, MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) < ANY
(SELECT salary
FROM employees
WHERE department_id IN (10,20))
ORDER BY department_id;

SELECT employee_id, manager_id, department_id
FROM employees
WHERE(manager_id,department_id) IN
(SELECT manager_id,department_id
FROM employees
WHERE employee_id IN (149,174))
AND employee_id NOT IN (149,174);

SELECT employee_id, manager_id, department_id
FROM employees
    WHERE manager_id IN
        (SELECT manager_id
        FROM employees
        WHERE employee_id IN
        (149,174))
    AND department_id IN
        (SELECT department_id
        FROM employees
        WHERE employee_id IN (149,174))
    AND employee_id NOT IN(149,174);
    
    
    SELECT o.first_name,
o.last_name,
o.salary
FROM employees o
WHERE o.salary >
(SELECT AVG(i.salary)
FROM employees i
WHERE i.department_id =
o.department_id);

SELECT employee_id,last_name AS "Not a Manager", manager_id
FROM employees emp
WHERE NOT EXISTS
(SELECT *
FROM employees mgr
WHERE mgr.manager_id = emp.employee_id);

With
SELECT employee_id,last_name AS "Is a Manager", manager_id
FROM employees emp
WHERE EXISTS
(SELECT *
FROM employees mgr
WHERE mgr.manager_id = emp.employee_id);


WITH managers AS
(SELECT DISTINCT manager_id
FROM employees
WHERE manager_id IS NOT NULL)
SELECT last_name AS "Not a manager"
FROM employees
WHERE employee_id NOT IN
(SELECT *
FROM managers);