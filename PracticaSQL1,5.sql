SELECT TO_CHAR(salary,
'$99,999') AS "Salary"
FROM employees;

SELECT TO_DATE('Noviembre 3, 2001', 'Month dd, yyyy')
FROM Dual;

SELECT last_name, TO_CHAR(hire_date, 'DD-Mon-YYYY')
FROM employees
WHERE hire_date < TO_DATE('01-Ene-90','DD-Mon-YY');

SELECT TO_CHAR(NEXT_DAY(ADD_MONTHS(hire_date, 6), 'VIERNES'), 'fmDay,
Month ddth, YYYY') AS "Next Evaluation"
FROM employees
WHERE employee_id = 100;

SELECT commission_pct, NVL(commission_pct, 0) as Comisioon
FROM employees



SELECT last_name, NVL(commission_pct, 0)*250
AS "Commission"
FROM employees
WHERE department_id IN(80,90);


SELECT last_name, salary,
NVL2(commission_pct, salary + (salary * commission_pct), salary)
AS income
FROM employees
WHERE department_id IN(80,90);

SELECT first_name, LENGTH(first_name) AS "Length FN", last_name,
LENGTH(last_name) AS "Length LN", NULLIF(LENGTH(first_name),
LENGTH(last_name)) AS "Compare Them"
FROM employees;

SELECT last_name,
COALESCE(commission_pct, manager_id, 10)
AS "Comm"
FROM employees
ORDER BY commission_pct;
