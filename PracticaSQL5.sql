SELECT ROUND (AVG(salary),2)
FROM employees
WHERE department_id= 90;

SELECT COUNT(job_id)
FROM employees;

SELECT DISTINCT(job_id)
FROM employees;

SELECT department_id, AVG(salary)
FROM employees
GROUP BY department_id
ORDER BY department_id;

SELECT COUNT (*) AS NumeroDeRegistros,department_id, MAX(salary)
FROM employees
GROUP BY department_id
ORDER BY department_id;

SELECT department_id, job_id, count(*)
FROM employees
WHERE department_id > 40
GROUP BY department_id, job_id
ORDER BY department_id, job_id;

SELECT max(avg(salary))
FROM employees
GROUP by department_id
ORDER by department_id;

SELECT department_id,MIN(salary)
FROM employees
GROUP BY department_id
HAVING COUNT(*)<= 2
ORDER BY department_id;

/*
Muestra el id de departamento y el salario promedio m�s alto
de ellos.
*/
SELECT department_id, AVG(salary) AS promedio
FROM employees
WHERE ROWNUM = 1
GROUP BY department_id
ORDER BY promedio DESC;

SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY ROLLUP (department_id, job_id);

SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY CUBE (department_id, job_id);

SELECT department_id, job_id, manager_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY GROUPING SETS ((job_id, manager_id), (department_id, job_id), (department_id, manager_id));

SELECT department_id, job_id, SUM(salary),
GROUPING(department_id) AS "Dept sub total",
GROUPING(job_id) AS "Job sub total"
FROM employees
WHERE department_id < 50
GROUP BY CUBE (department_id, job_id);


SELECT hire_date, employee_id, job_id
FROM employees
UNION
SELECT TO_DATE(NULL),employee_id, job_id
FROM job_history
order by employee_id;

SELECT hire_date, employee_id, TO_DATE(null) start_date,
TO_DATE(null) end_date, job_id, department_id
FROM employees
UNION
SELECT TO_DATE(null), employee_id, start_date, end_date, job_id,
department_id
FROM job_history
ORDER BY employee_id;

SELECT first_name, last_name, hire_date
FROM employees
WHERE last_name = 'King';


SELECT first_name, last_name,
hire_date
FROM employees
WHERE hire_date >
(SELECT hire_date
FROM employees
WHERE (last_name = 'King') and  (first_name ='Steven'));


SELECT last_name, job_id, department_id
FROM employees
WHERE department_id =
(SELECT department_id
FROM departments
WHERE department_name = 'Marketing')
ORDER BY job_id;


SELECT last_name, job_id, department_id
FROM employees
WHERE job_id =
(SELECT job_id
FROM employees
WHERE employee_id = 141)
AND department_id=
(SELECT department_id
FROM departments
WHERE location_id = 1500);



SELECT department_id, MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) >
(SELECT MIN(salary)
FROM employees
WHERE department_id= 50)
ORDER BY department_id;

SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(YEAR FROM hire_date) IN
(SELECT EXTRACT(YEAR FROM hire_date)
FROM employees
WHERE department_id=90);