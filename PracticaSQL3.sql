SELECT last_name,department_id,
CASE department_id
WHEN 90 THEN 'Management'
WHEN 80 THEN 'Sales'
WHEN 60 THEN 'It'
ELSE 'Other dept.'
END AS "Department"
FROM employees;


SELECT last_name,
DECODE(department_id,
90, 'Management',
80, 'Sales',
60, 'It',
'Other dept.')
AS "Department"
FROM employees;



SELECT first_name, last_name, job_id, job_title
FROM employees NATURAL JOIN jobs
WHERE department_id > 80;

SELECT e.first_name, e.last_name, e.job_id, j.job_title
FROM employees e, jobs j
WHERE e.job_id = j.job_id 
AND e.department_id > 80;


SELECT last_name, department_name
FROM employees CROSS JOIN departments
WHERE last_name = 'Abel';