INSERT INTO copy_employees
VALUES (1, 'Natacha', 'Hansen', 'NHANSEN', '4412312341234',
'07-SEP-1998', 'AD_VP', 12000, null, 100, 90, NULL);

SELECT employee_id,first_name ||' '|| last_name AS "NAME",
versions_operation AS "OPERATION",
versions_starttime AS "START_DATE",
versions_endtime AS "END_DATE", salary
FROM copy_employees
VERSIONS BETWEEN SCN MINVALUE AND MAXVALUE
WHERE employee_id = 1;

UPDATE copy_employees
SET salary = 1
WHERE employee_id= 1;

SELECT employee_id,first_name ||' '|| last_name AS "NAME",
versions_operation AS "OPERATION",
versions_starttime AS "START_DATE",
versions_endtime AS "END_DATE", salary
FROM copy_employees
VERSIONS BETWEEN SCN MINVALUE AND MAXVALUE
WHERE employee_id = 1;