MERGE INTO copy_employees c USING employees e
ON (c.employee_id = e.employee_id)
WHEN MATCHED THEN UPDATE
SET c.last_name = e.last_name, c.department_id = e.department_id
WHEN NOT MATCHED THEN 
INSERT(employee_id, last_name, department_id)
VALUES (e.employee_id, e.last_name, e.department_id);


INSERT ALL
INTO my_employees
VALUES (hire_date, first_name, last_name)
INTO copy_my_employees
VALUES (hire_date, first_name, last_name)
SELECT hire_date, first_name, last_name
FROM employees;


CREATE TABLE my_friends
(first_name VARCHAR2(20),
last_name VARCHAR2(30),
email VARCHAR2(30),
phone_num VARCHAR2(12),
birth_date DATE);

SELECT table_name, status
FROM USER_TABLES;


SELECT table_name, status
FROM ALL_TABLES;

SELECT *
FROM user_indexes;

SELECT *
FROM user_objects
WHERE object_type= 'SEQUENCE';